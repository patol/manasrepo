<%-- 
    Document   : AdminHome
    Created on : 4 Feb, 2015, 12:56:11 PM
    Author     : parth
--%>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    
    <jsp:include page="commonJSP/commonHead.jsp"></jsp:include>
    
    <%@taglib prefix="s" uri="/struts-tags"%>
    
</head>

<body>

    <div id="wrapper">

      <jsp:include page="commonJSP/commonBodyTop.jsp"></jsp:include>  
			
			
        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12" style="border: 5px blue solid">
                      <!-- Put the admin Page content here -->  
											
                      <h1><s:property value ="admin_pojo.email" /></h1>							
											
											
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

			
			
    </div>
    <!-- /#wrapper -->

    <jsp:include page="commonJSP/commonBodyBottom.jsp"></jsp:include>

</body>

</html>