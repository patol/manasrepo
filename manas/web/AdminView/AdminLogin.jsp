<%-- 
    Document   : AdminLogin
    Created on : 4 Feb, 2015, 11:12:59 AM
    Author     : parth
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    
        <s:form action="admin_login" method="post">
            <s:textfield name="admin_pojo.email" label="E-mail:"size="20" />
            <s:password name="admin_pojo.password" label="Password:"  size="20" />
            <s:submit method="execute" label="Login" align="center" />
        </s:form>
        
    
</html>
