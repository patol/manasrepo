/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package admin;

import com.opensymphony.xwork2.ActionSupport;

/**
 *
 * @author parth
 */
public class AdminLoginAction extends ActionSupport {
    
    private AdminPojo admin_pojo;
    
    
    
    public AdminLoginAction() {
    }
    
    @Override
    public String execute() throws Exception {
       
        System.out.println("This validates of form fill-upped object :"+admin_pojo.getEmail());
        
        boolean flag = AdminDAO.verify(admin_pojo);
        
        if(flag)
            return "success";
        else
            return "backtologin";
        
        
    }

   
    // getter setter for AdminPojo bean
    //---start
     public AdminPojo getAdmin_pojo() {
        return admin_pojo;
    }

    public void setAdmin_pojo(AdminPojo admin_pojo) {
        this.admin_pojo = admin_pojo;
    }
    //--end
}
