/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package general.DAO;


import general.hibernate.utilities.HibernateUtil;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
//import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author parth
 */
public class GeneralDAO {

    
    // adding object into table
    public Object add() {
        Object flag = null;
        try {
            Session session = HibernateUtil.getSessionFactory().openSession();
            Transaction tx = session.beginTransaction();
            session.save(this);
            tx.commit();
            flag = this;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return flag;
    }

    
    // delete object by id from table
    public boolean delete(Long id) {
        boolean flag = false;
        try {
            Session session = HibernateUtil.getSessionFactory().openSession();
            Transaction tx = session.beginTransaction();
            Object obj = session.get(this.getClass(), id);

            session.delete(obj);
            tx.commit();
            flag = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return flag;
    }

    
    
    //updation of entry in table
    //  Usage:-- oldObject.update(Id of new Object) 
    public boolean update(Long id) {
        boolean flag = false;
        try {
            Session session = HibernateUtil.getSessionFactory().openSession();
            Transaction tx = session.beginTransaction();
            Object obj = session.get(this.getClass(), id);
            Field updated_fields[] = this.getClass().getDeclaredFields();
            Field fields_to_be_updated[] = obj.getClass().getDeclaredFields();

            for (Field f1 : updated_fields) {
                //System.out.println(“New Values :”+f1.getName());
                for (Field f2 : fields_to_be_updated) {
                    if (f1.getName().equals(f2.getName()) && !f1.getName().equals("id")) {
                        //System.out.println(“Set”);
                        f2.set(obj, f1.get(this));
                    }
                }
            }
            session.update(obj);
            tx.commit();
            flag = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return flag;
    }
    
    
    
    // Get table row by id 
    // Usage:- wanted_object.getbyId( id of object)
    public Object getById(Long id) {
        Object obj = null;
        try {
            Session session = HibernateUtil.getSessionFactory().openSession();
            obj = session.get(this.getClass(), id);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return obj;
    }
    
    
    public Object getByEmail(String email) {
        Object obj = null;
        try {
            Session session = HibernateUtil.getSessionFactory().openSession();
            Query q=session.createQuery("from "+this.getClass().getName()+" where email='"+email+"'");
            obj=q.list().get(0);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return obj;
    }
    
    
    public Object getByColumn(String ColumnName ,String value) {
        Object obj = null;
        try {
            Session session = HibernateUtil.getSessionFactory().openSession();
            Query q=session.createQuery("from "+this.getClass().getName()+" where  "+ColumnName+"='"+value+"'");
            obj=q.list().get(0);
            
        } catch (Exception e) {
            e.printStackTrace();
        }
        return obj;
    }
    
    
    
    
    
    // get list of all rows in table
    // Usage:- wanted_object.getListOfAll()
    public List<? extends Object> getListOfAll() {
        List<? extends Object> list = null;
        try {
            Session session = HibernateUtil.getSessionFactory().openSession();
            Query query = session.createQuery("from " + this.getClass().getName());
            list = query.list();
            
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }
    
    public Long getByName(String name) {
        Long id = null;
        try {
            Session session = HibernateUtil.getSessionFactory().openSession();
            id = (Long)session.get(this.getClass(), name);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return id;
        
    }
    
    
      
    public boolean approve(String object_feild_name) {
        List<? extends Object> list = null;
        boolean status = false;
        
        try {
           // Session session = HibernateUtil.getSessionFactory().openSession();
            Session session=HibernateUtil.getSessionFactory().openSession();
            Query query = session.createQuery("from " + object_feild_name+" where status='0'" );
            list = query.list();
            if(!list.isEmpty()){
                status = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return status;
    }
    
    
    
    
    public boolean  UpdatePassword(String password,Long id)
    {
        boolean b=false;
        try 
        {
            Session s=HibernateUtil.getSessionFactory().openSession();
            Transaction tx=s.beginTransaction();
       
            Query q=s.createQuery("update "+this.getClass().getName()+" set password='"+password+"'  where id="+id+"");

            q.executeUpdate();
            tx.commit();
            
            b=true;
            
        } 
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return b;
    }
    
    
}
